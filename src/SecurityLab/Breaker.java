package SecurityLab;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Breaker {

    private static byte[] hash = Passwords.base64Decode("6XZfhbfU8Il1CSHnO9NimnYMH+emHWLz1p08T+2T/8ZlXmhN6uCATkckXvdR8eBE9MlEQ4TfCDJ/ydjODM6c2g==");
    private static byte[] hash2=Passwords.base64Decode("jUwKgSbr5K0A8lk9Nq7uY2CIlflZmc8h9U5cDCeEEtwKKST6QEg6uMY+Gz11ytUDqmZM5gtwWrekaaxsQRHtPw==");
    private static Long time1;
//    private static Byte[][] bytes;

    private static void start() {
//        System.out.println(Byte.M);

        time1 = System.currentTimeMillis();

        String[] guesses = " abcdefghijklmnopqrstuvwxyz0123456789".split("");


        System.out.println(guessPassword(guesses) +" straight pw");
        System.out.println(guessSaltyPassword(guesses)+" salty pw");
    }

//

    private static String guessPassword(String[] guesses) {
        byte[] hashedGuess;
        String password = "nope";

        for (String a:guesses) {
            for (String b : guesses
            ) {
                for (String c : guesses
                ) {
                    for (String d : guesses
                    ) {
                        String pw = (a + b + c + d).trim();
                        if (Passwords.isInsecureHashMatch(pw, hash)) {
                            password = pw;
                            System.out.println(((System.currentTimeMillis() - time1) / 1000.0) + " sec");
                            return password;
                        }
                    }
                }
            }
        }
        return password;
    }

    private static byte[][] getBytes(){

        int num = Byte.MAX_VALUE + Math.abs(Byte.MIN_VALUE) +1;
        byte[][] salts = new byte[num][];
        for (int i = 0; i < salts.length; i++) {
            byte next = Byte.parseByte("" +(Byte.MIN_VALUE + i));
            salts[i] = new byte[]{next};
        }
System.out.println("bytes done");
        return salts;
    }

    private static String guessSaltyPassword(String[] guesses) {
//        byte[] hashedGuess;
        String password = "nope";
        byte[][] bytes = getBytes();
        for (String a:guesses) {
            for (String b : guesses
            ) {
                for (String c : guesses
                ) {
                    for (String d : guesses
                    ) {
                        String pw = (a + b + c + d).trim();

//            hashedGuess = Passwords.insecureHash(list.get(i));

                        for (byte[] salt:bytes
                             ) {
                            if (Passwords.isExpectedPassword(pw.toCharArray(), salt, 5, hash2)) {
                                password = pw;
                                System.out.println(((System.currentTimeMillis() - time1) / 1000.0) + " sec");
                                return password;
                        }

                        }
                    }
                }
            }
        }


        return password;
    }


    public static void breakSalty(){
        time1 = System.currentTimeMillis();

    }

    public static void main(String[] args) {
        Breaker.start();
//        Breaker.breakSalty();
    }
}
